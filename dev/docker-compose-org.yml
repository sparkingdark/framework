# docker-compose-org.yml is a reference file to create a docker-compose.yml.
#
# Image_Type : {infrastructure, app, extra}
# Image_Name : {no_format, app_APPNAME, no_format}
#
# To add a configuration :
# 1) Add new image name in /dev/config.json.dist.
# 2) Add image configuration to this file and make sure to add
# '# Image--{Image_Type}:{Image_Name}'
# before comments and defining configuration (as shown in example below).
#
# ex.
# # Image--infrastructure:nginx
# Comments Related to Image
# Image Configuration

version: '3.5'
services:
  #
  # Infrastructure
  #
  # Image--infrastructure:nginx
  nginx:
    image: nginx:1.13.9-alpine
    networks:
      - nodelocal
      - nodelocal-private
    ports:
      - '127.0.0.1:2354:8080'
    volumes:
      - type: bind
        source: ./conf/nginx.conf
        target: /etc/nginx/nginx.conf
        read_only: true
      - type: bind
        source: ../
        target: /srv/
        read_only: true
      - type: bind
        source: ./stubs/dl/
        target: /srv/webroot/dl/
        read_only: true

  # Image--infrastructure:redis
  redis:
    image: redis:4.0.8-alpine
    networks:
      - nodelocal-private
    volumes:
      - type: volume
        source: redis
        target: /data

  # Image--infrastructure:mariadb
  mariadb:
    image: mariadb:10.2.13
    hostname: mariadb
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'
    networks:
      - nodelocal-private
    ports:
      - '127.0.0.1:23306:3306'
    volumes:
      - type: bind
        source: ./conf/mariadb.cnf
        target: /etc/mysql/conf.d/docker.cnf
        read_only: true
      - type: volume
        source: mariadb
        target: /var/lib/mysql

  # Image--infrastructure:pma
  pma:
    image: phpmyadmin/phpmyadmin:4.7.9-1
    environment:
      PMA_HOST: mariadb
    networks:
      - nodelocal-private
    ports:
      - '127.0.0.1:2355:80'
    volumes:
      - type: bind
        source: ./conf/pma.config.user.inc.php
        target: /etc/phpmyadmin/config.user.inc.php
        read_only: true

  # Image--infrastructure:orchestrator
  orchestrator:
    image: registry.gitlab.com/cdli/framework/orchestrator:0.0.3
    hostname: orchestrator
    networks:
      - nodelocal
      - nodelocal-private
    volumes:
      - type: bind
        source: ./stubs/orchestrator/
        target: /srv/orchestrator/
        read_only: true

  # Image--infrastructure:fuseki
  fuseki:
    image: stain/jena-fuseki
    volumes:
      - fuseki_data:/fuseki
    ports:
      - 3030:3030
    environment:
      - ADMIN_PASSWORD=pw123
  
  # Image--infrastructure:elasticsearch
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.8.0
    hostname: elasticsearch
    environment:
      - cluster.name=es-cluster
      - node.name=elasticsearch
      - discovery.type=single-node
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.license.self_generated.type=basic
      - xpack.monitoring.collection.enabled=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
      - "ELASTIC_PASSWORD=elasticchangeme"
    # command: elasticsearch-setup-passwords -b auto
    ulimits:
      nofile: 65536
      memlock:
        soft: -1
        hard: -1
    volumes:
      - type: bind
        source: ../elastic_search/data
        target: /usr/share/elasticsearch/data
    ports:
      - '127.0.0.1:9200:9200'
      - '127.0.0.1:9300:9300'
    networks:
      - nodelocal-private
    depends_on:
      - mariadb
  
  # Image--infrastructure:kibana
  kibana:
    hostname: kibana
    image: docker.elastic.co/kibana/kibana:7.8.0
    environment:
      - elasticsearch.url="http://elasticsearch:9200"
      - server.name="kibana"
      - xpack.security.enabled=true
      - ELASTICSEARCH_USERNAME=elastic
      - ELASTICSEARCH_PASSWORD=elasticchangeme
    ports:
    - '127.0.0.1:5601:5601'
    networks:
    - nodelocal-private
    depends_on:
    - elasticsearch

  # Image--infrastructure:logstash
  logstash:
    image: docker.elastic.co/logstash/logstash:7.8.0
    hostname: logstash
    environment:
      - "LS_JAVA_OPTS=-Xmx1024m -Xms1024m"
    volumes:
      - type: bind
        source: ../logstash/config/logstash.yml
        target: /usr/share/logstash/config/logstash.yml
      - type: bind
        source: ../logstash/pipeline/logstash.conf
        target: /usr/share/logstash/pipeline/logstash.conf
      - type: bind
        source: ../logstash/drivers/mariadb-java-client-2.6.1.jar
        target: /usr/share/logstash/logstash-core/lib/jars/mariadb-java-client-2.6.1.jar
    ports:
      - "5001:5001"
    networks:
      - nodelocal-private
    depends_on:
      - mariadb
      - elasticsearch

  # Image--infrastructure:elasticsearch
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.8.0
    hostname: elasticsearch
    environment:
      - cluster.name=es-cluster
      - node.name=elasticsearch
      - discovery.type=single-node
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.license.self_generated.type=basic
      - xpack.monitoring.collection.enabled=true
      - "ES_JAVA_OPTS=-Xms2g -Xmx2g"
      - "ELASTIC_PASSWORD=elasticchangeme"
    ulimits:
      nofile: 65536
      memlock:
        soft: -1
        hard: -1
    volumes:
      - type: bind
        source: ../elastic_search/data
        target: /usr/share/elasticsearch/data
    ports:
      - '127.0.0.1:9200:9200'
      - '127.0.0.1:9300:9300'
    networks:
      - nodelocal-private
    depends_on:
      - mariadb

  # Image--infrastructure:kibana
  kibana:
    hostname: kibana
    image: docker.elastic.co/kibana/kibana:7.8.0
    environment:
      - elasticsearch.url="http://elasticsearch:9200"
      - server.name="kibana"
      - xpack.security.enabled=true
      - ELASTICSEARCH_USERNAME=elastic
      - ELASTICSEARCH_PASSWORD=elasticchangeme
    ports:
      - '127.0.0.1:5601:5601'
    networks:
      - nodelocal-private
    depends_on:
      - elasticsearch

  # Image--infrastructure:logstash
  logstash:
    image: docker.elastic.co/logstash/logstash:7.8.0
    hostname: logstash
    environment:
      - 'LS_JAVA_OPTS=-Xmx1024m -Xms1024m'
    volumes:
      - type: bind
        source: ../logstash/config/logstash.yml
        target: /usr/share/logstash/config/logstash.yml
      - type: bind
        source: ../logstash/pipeline/logstash.conf
        target: /usr/share/logstash/pipeline/logstash.conf
      - type: bind
        source: ../logstash/drivers/mariadb-java-client-2.6.1.jar
        target: /usr/share/logstash/logstash-core/lib/jars/mariadb-java-client-2.6.1.jar
    ports:
      - '5001:5001'
    networks:
      - nodelocal-private
    depends_on:
      - mariadb
      - elasticsearch

  #
  # Apps
  #
  # Image--app:cake
  app_cake:
    image: registry.gitlab.com/cdli/framework/app_cake:0.5.0-rc.2
    hostname: app_cake
    networks:
      - nodelocal-private
    volumes:
      - type: bind
        source: ../app/cake/
        target: /srv/app/cake/
      - type: bind
        source: ../app/private/
        target: /srv/app/private/
      - type: bind
        source: ../app/tools/upload/
        target: /srv/app/tools/upload/
      #  - type: bind
      #    source: ../app/collections/
      #    target: /srv/app/collections/
      - type: bind
        source: ./stubs/cdli_archivalfiles_new/
        target: /mnt/cdli_archivalfiles_new/
        read_only: true
      - type: volume
        source: cdli_collections
        target: /mnt/cdli_collections_new
      - type: bind
        source: ./stubs/dl/
        target: /srv/app/cake/webroot/dl/
        read_only: true

  # Image--extra:commodity-api
  commodity-api:
    image: registry.gitlab.com/cdli/framework/commodity-api:latest
    hostname: commodity-api
    ports:
      - 8087:8088
    environment:
      - FLASK_ENV= development
    depends_on:
      - mariadb
    networks:
      - nodelocal-private

  # Image--extra:commodity-viz
  commodity-viz:
    image: registry.gitlab.com/cdli/framework/commodity-viz:latest
    hostname: commodity-viz
    depends_on:
      - commodity-api
    ports:
      - 4001:80

  # Image--app:cqp4rdf
  app_cqp4rdf:
    image: registry.gitlab.com/cdli/framework/app_cqp4rdf:latest
    hostname: app_cqp4rdf
    ports:
      - 8088:8088
    depends_on:
      - fuseki
    environment:
      - FLASK_ENV= development

  #
  # Extra
  #
  # Image--extra:python2-tools
  python2-tools:
    build:
      context: ../app/tools
      dockerfile: Dockerfile-py2
    container_name: py2-tools
    command: bash -c "pip install /srv/app/tools/atf2conll-convertor /srv/app/tools/conllu.py && tail -F anything"
    volumes:
      - type: bind
        source: ../app/tools/atf2conll-convertor
        target: /srv/app/tools/atf2conll-convertor
      - type: bind
        source: ../app/tools/conllu.py
        target: /srv/app/tools/conllu.py

  # Image--extra:python3-tools
  python3-tools:
    build:
      context: ../app/tools
      dockerfile: Dockerfile-py3
    container_name: py3-tools
    command:
      bash -c "pip install /srv/app/tools/pyoracc /srv/app/tools/morphology-pre-annotation-tool
      /srv/app/tools/CDLI-CoNLL-to-CoNLLU-Converter /srv/app/tools/brat_to_cdli_conll_converter && tail -F anything"
    volumes:
      - type: bind
        source: ../app/tools/pyoracc
        target: /srv/app/tools/pyoracc
      - type: bind
        source: ../app/tools/morphology-pre-annotation-tool
        target: /srv/app/tools/morphology-pre-annotation-tool
      - type: bind
        source: ../app/tools/CDLI-CoNLL-to-CoNLLU-Converter
        target: /srv/app/tools/CDLI-CoNLL-to-CoNLLU-Converter
      - type: bind
        source: ../app/tools/brat_to_cdli_conll_converter
        target: /srv/app/tools/brat_to_cdli_conll_converter

  # Image--extra:node-tools
  node-tools:
    build:
      context: ../app/tools
      dockerfile: Dockerfile-node
    container_name: node-tools
    networks:
      - nodelocal-private
    command: bash -c "cd /srv/app/tools/converter-service && npm start"
    volumes:
      - type: bind
        source: ../app/tools/CDLI-CoNLL-to-CoNLLU-Converter
        target: /srv/app/tools/CoNLL-U
      - type: bind
        source: ../app/tools/CoNLL-RDF
        target: /srv/app/tools/CoNLL-RDF
      - type: bind
        source: ../dev/assets/csl-styles
        target: /srv/app/tools/converter-service/styles
      - type: bind
        source: ../app/tools/converter-service/src
        target: /srv/app/tools/converter-service/src

  # Image--extra:scripts
  # Container for apps requiring accessing internet (Example: fetching emails and saving to database)
  scripts:
    build:
      context: ../app/tools/scripts
      dockerfile: Dockerfile
    container_name: scripts
    networks:
      - nodelocal-private
      - nodelocal
    volumes:
      - type: bind
        source: ../app/tools/scripts
        target: /srv
    depends_on:
      - mariadb

  # Image--extra:dev_cake
  dev_cake:
    build:
      context: ../app/cake
      dockerfile: Dockerfile_composer
    container_name: dev_cake_composer
    networks:
      - nodelocal-private
      - nodelocal
    volumes:
      - type: bind
        source: ../
        target: /srv/
    command: sh -c 'composer install -n -d /srv/app/cake;composer update -n -d /srv/app/cake; tail -f /dev/null;'
    tty: true
    depends_on:
      - app_cake

#
# Data volume definitions
#
volumes:
  mariadb:
  redis:
  cdli_collections:
  fuseki_data:
  elastic_search:
  logstash:

#
# Network definitions
#
networks:
  nodelocal:
    driver: bridge
    driver_opts:
      com.docker.network.bridge.enable_icc: 'true'
      com.docker.network.bridge.enable_ip_masquerade: 'true'
  nodelocal-private:
    driver: bridge
    driver_opts:
      com.docker.network.bridge.enable_icc: 'true'
      com.docker.network.bridge.enable_ip_masquerade: 'false'
