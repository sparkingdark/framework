<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Dynasties Controller
 *
 * @property \App\Model\Table\DynastiesTable $Dynasties
 *
 * @method \App\Model\Entity\Dynasty[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DynastiesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'contain' => ['Proveniences']
        ];
        $dynasties = $this->paginate($this->Dynasties);

        $this->set(compact('dynasties'));
    }

    /**
     * View method
     *
     * @param string|null $id Dynasty id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $dynasty = $this->Dynasties->get($id, [
            'contain' => ['Proveniences', 'Dates', 'Rulers']
        ]);

        $this->set('dynasty', $dynasty);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $dynasty = $this->Dynasties->newEntity();
        if ($this->request->is('post')) {
            $dynasty = $this->Dynasties->patchEntity($dynasty, $this->request->getData());
            if ($this->Dynasties->save($dynasty)) {
                $this->Flash->success(__('The dynasty has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dynasty could not be saved. Please, try again.'));
        }
        $proveniences = $this->Dynasties->Proveniences->find('list', ['limit' => 200]);
        $this->set(compact('dynasty', 'proveniences'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dynasty id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $dynasty = $this->Dynasties->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dynasty = $this->Dynasties->patchEntity($dynasty, $this->request->getData());
            if ($this->Dynasties->save($dynasty)) {
                $this->Flash->success(__('The dynasty has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dynasty could not be saved. Please, try again.'));
        }
        $proveniences = $this->Dynasties->Proveniences->find('list', ['limit' => 200]);
        $this->set(compact('dynasty', 'proveniences'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dynasty id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $dynasty = $this->Dynasties->get($id);
        if ($this->Dynasties->delete($dynasty)) {
            $this->Flash->success(__('The dynasty has been deleted.'));
        } else {
            $this->Flash->error(__('The dynasty could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
