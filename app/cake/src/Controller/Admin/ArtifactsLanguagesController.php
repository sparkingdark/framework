<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ArtifactsLanguages Controller
 *
 * @property \App\Model\Table\ArtifactsLanguagesTable $ArtifactsLanguages
 *
 * @method \App\Model\Entity\ArtifactsLanguage[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsLanguagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Languages']
        ];
        $artifactsLanguages = $this->paginate($this->ArtifactsLanguages);

        $this->set(compact('artifactsLanguages'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Language id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsLanguage = $this->ArtifactsLanguages->get($id, [
            'contain' => ['Artifacts', 'Languages']
        ]);

        $this->set('artifactsLanguage', $artifactsLanguage);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsLanguage = $this->ArtifactsLanguages->newEntity();
        if ($this->request->is('post')) {
            $artifactsLanguage = $this->ArtifactsLanguages->patchEntity($artifactsLanguage, $this->request->getData());
            if ($this->ArtifactsLanguages->save($artifactsLanguage)) {
                $this->Flash->success(__('The artifacts language has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts language could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsLanguages->Artifacts->find('list', ['limit' => 200]);
        $languages = $this->ArtifactsLanguages->Languages->find('list', ['limit' => 200]);
        $this->set(compact('artifactsLanguage', 'artifacts', 'languages'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Language id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsLanguage = $this->ArtifactsLanguages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsLanguage = $this->ArtifactsLanguages->patchEntity($artifactsLanguage, $this->request->getData());
            if ($this->ArtifactsLanguages->save($artifactsLanguage)) {
                $this->Flash->success(__('The artifacts language has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts language could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsLanguages->Artifacts->find('list', ['limit' => 200]);
        $languages = $this->ArtifactsLanguages->Languages->find('list', ['limit' => 200]);
        $this->set(compact('artifactsLanguage', 'artifacts', 'languages'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Language id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsLanguage = $this->ArtifactsLanguages->get($id);
        if ($this->ArtifactsLanguages->delete($artifactsLanguage)) {
            $this->Flash->success(__('The artifacts language has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts language could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
