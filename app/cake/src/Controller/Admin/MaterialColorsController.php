<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * MaterialColors Controller
 *
 * @property \App\Model\Table\MaterialColorsTable $MaterialColors
 *
 * @method \App\Model\Entity\MaterialColor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaterialColorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialColors = $this->paginate($this->MaterialColors);

        $this->set(compact('materialColors'));
    }

    /**
     * View method
     *
     * @param string|null $id Material Color id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialColor = $this->MaterialColors->get($id, [
            'contain' => ['ArtifactsMaterials']
        ]);

        $this->set('materialColor', $materialColor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialColor = $this->MaterialColors->newEntity();
        if ($this->request->is('post')) {
            $materialColor = $this->MaterialColors->patchEntity($materialColor, $this->request->getData());
            if ($this->MaterialColors->save($materialColor)) {
                $this->Flash->success(__('The material color has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The material color could not be saved. Please, try again.'));
        }
        $this->set(compact('materialColor'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Material Color id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $materialColor = $this->MaterialColors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $materialColor = $this->MaterialColors->patchEntity($materialColor, $this->request->getData());
            if ($this->MaterialColors->save($materialColor)) {
                $this->Flash->success(__('The material color has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The material color could not be saved. Please, try again.'));
        }
        $this->set(compact('materialColor'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Material Color id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $materialColor = $this->MaterialColors->get($id);
        if ($this->MaterialColors->delete($materialColor)) {
            $this->Flash->success(__('The material color has been deleted.'));
        } else {
            $this->Flash->error(__('The material color could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
