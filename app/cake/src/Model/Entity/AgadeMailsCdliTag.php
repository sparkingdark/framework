<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AgadeMailsCdliTag Entity
 *
 * @property int $id
 * @property int $agade_mail_id
 * @property int $cdli_tag_id
 *
 * @property \App\Model\Entity\AgadeMail $agade_mail
 * @property \App\Model\Entity\CdliTag $cdli_tag
 */
class AgadeMailsCdliTag extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'agade_mail_id' => true,
        'cdli_tag_id' => true,
        'agade_mail' => true,
        'cdli_tag' => true
    ];
}
