<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsShadow Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $cdli_comments
 * @property string|null $collection_location
 * @property string|null $collection_comments
 * @property string|null $acquisition_history
 *
 * @property \App\Model\Entity\Artifact $artifact
 */
class ArtifactsShadow extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'cdli_comments' => true,
        'collection_location' => true,
        'collection_comments' => true,
        'acquisition_history' => true,
        'artifact' => true
    ];
}
