<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="capital-heading text-center">Are you sure you want to log out?</div>
            <?= $this->Flash->render() ?>
        <div class="d-flex justify-content-around">
            <?= $this->Form-> create ('Logout', [
                'url' => [
                    'controller' => 'Logout',
                    'action' => 'index']
                ]); ?>
                <?= $this->Form->submit('Logout', ['class' => 'form-control btn btn-primary ml-5']); ?>
            <?= $this->Form->end() ?>
            <?= $this->Form-> create ('Cancel', [
                'url' => [
                    'action' => 'cancel']
                ]); ?>
                <?= $this->Form->submit('Cancel', ['class' => 'form-control btn btn-primary mr-5']); ?>
            <?= $this->Form->end() ?>
        </div>
        </div>
    </div>
</div>
