<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication $artifactsPublication
 */
?>
<h1 class="display-3 header-text text-left"><?= __('Edit Artifact-Publication Link') ?></h1>

<?= $this->cell('PublicationView', [$artifactsPublication->publication->id]) ?> 
<?= $this->cell('ArtifactView', [$artifactsPublication->artifact_id]); ?>

<div class="row justify-content-md-center ads">
    <div class="col-lg boxed">
        <legend class="capital-heading"><?= __('Edit link for Artifact P'.substr("00000{$artifactsPublication->artifact_id}", -6).' and the Publication') ?></legend>
        <?= $this->Form->create($artifactsPublication, ['action' => 'edit/'.$artifactsPublication->id.'/'.$flag.'/'.$parent_id]) ?>
            <?php echo $this->Form->control('publication_id', ['type' => 'hidden', 'value' => $artifactsPublication->publication->bibtexkey]); ?>
            <?php echo $this->Form->control('artifact_id', ['type' => 'hidden']); ?>
            <div class="layout-grid text-left">
                <div>
                    Publication Comments:
                    <?= $this->Form->control('publication_comments', ['label' => false, 'type' => 'textarea']); ?>
                </div>

                <div>
                    Exact Reference:
                    <?= $this->Form->control('exact_reference', ['label' => false, 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?>
                    Publication Type:
                    <?php $options = [
                        'primary' => 'primary',
                        'electronic' => 'electronic',
                        'citation' => 'citation',
                        'collation' => 'collation',
                        'history' => 'history',
                        'other' => 'other'
                        ];
                    echo $this->Form->control('publication_type', ['label' => false, 'type' => 'select', 'options' => $options, 'class' => 'form-select'] );?>
                </div>
            </div>
        <?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
