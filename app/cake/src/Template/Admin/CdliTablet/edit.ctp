<?php 
/*
 * Display date in view without formatting
 * 
*/
Cake\I18n\Date::setToStringFormat('YYYY-MM-dd');
Cake\I18n\FrozenDate::setToStringFormat('YYYY-MM-dd');
    
\Cake\Database\Type::build('date')
    ->useImmutable()
    ->useLocaleParser()
    ->setLocaleFormat('YYYY-MM-dd');
?>

<div class="container">

<h1 class="display-3 text-left header-txt">CDLI tablet</h1>
    <p class="text-left home-desc mt-4">Web admin interface for data entry and management.</p>
    <div class="card">
        <div class="card-body">
        <!-- Back and Add buttons -->
        <table>
            <form class="form-inline">
                <tbody>
                    <td>
                        <?php echo $this->Html->link(
                            '<< Back', 
                            array('controller'=>'CdliTablet', 'action'=>'index'), 
                            array('class'=>'btn btn-dark'));
                        ?>
                    </td>
                   
                </tbody>
            </form>
        </table>
<div class="row justify-content-md-center">

<div class="col-lg-7 boxed">
    <legend class="capital-heading row justify-content-md-center"><?= __('Edit an existing entry') ?></legend>
    <?php 
        echo $this->Form->create(NULL, array('url'=>'admin/CdliTablet/edit/'.$id));    
    ?>
    <form method="post" accept-charset="utf-8" role="form" action="#">
        <table cellpadding="10" cellspacing="10">
        <div style="display:none;">
            <input type="hidden" name="_method" class="form-control"  value="POST" />
        </div>
            <tr>
            <div class="form-group row form-group text">
                <div class="col-sm-10">
                    <?php echo $this->Form->input('Date (required)', ['value'=>$displaydate, 'class'=>'form-control', 'type'=>'text', 'name'=>'displaydate', 'id'=>'displaydate', 'required'=>true]) ?>
                </div>
            </div>
            </tr>
            <tr>
            <div class="form-group row form-group text">
                <div class="col-sm-10">
                    <?php echo $this->Form->input('Theme (optional)', ['value'=>$theme, 'class'=>'form-control', 'type'=>'text', 'name'=>'theme', 'id'=>'theme', 'required'=>false]) ?>
                </div>
            </div>
            </tr>
            <tr>
            <div class="form-group row form-group text">
                <div class="col-sm-10">
                    <?php echo $this->Form->input('Short Title (required)', ['value'=>$shorttitle, 'class'=>'form-control', 'type'=>'text', 'name'=>'shorttitle', 'id'=>'shorttitle', 'required'=>true]) ?>
                </div>
            </div>
            </tr>
            <tr>
            <div class="form-group row form-group text">
                <div class="col-sm-10">
                    <?php echo $this->Form->input('Long Title (optional)', ['value'=>$longtitle, 'class'=>'form-control', 'type'=>'text', 'name'=>'longtitle', 'id'=>'longtitle', 'required'=>false]) ?>
                </div>
            </div>
            </tr>
            <tr>
            <div class="form-group row form-group text">
                <label class="col-sm-3 col-form-label control-label" for="shortdesc">Short Description (required)</label>
                <div class="col-sm-10">
                    <?php echo $this->Form->textarea('Short Description (required)', ['value'=>$shortdesc, 'class'=>'form-control', 'type'=>'text', 'name'=>'shortdesc', 'id'=>'shortdesc', 'required'=>true]) ?>
                </div>
                <div class="col-sm-10">
                    <small id="shortdesc" class="form-text text-muted">Description should be from 10-50 words [UTF8 & HTML are supported].</small>
                </div>
            </div>
            </tr>
            <tr>
            <div class="form-group row form-group text">
                <label class="col-sm-3 col-form-label control-label" for="longdesc">Long Description (required)</label>
                <div class="col-sm-10">
                    <?php echo $this->Form->textarea('Long Description (required)', ['value'=>$longdesc, 'class'=>'form-control', 'type'=>'text', 'name'=>'longdesc', 'id'=>'longdesc', 'required'=>true]) ?>
                </div>
                <div class="col-sm-10">
                    <small id="longdesc" class="form-text text-muted">Description should be from 10-50 words [UTF8 & HTML are supported].</small>
                </div>
            </div>
            </tr>
            <tr>
            <div class="form-group row form-group text">
                <div class="col-sm-10">
                    <?php echo $this->Form->input('Created by (required)', ['value'=>$createdby, 'class'=>'form-control', 'type'=>'text', 'name'=>'createdby', 'id'=>'createdby', 'required'=>true]) ?>
                </div>
            </div>
            </tr>
        </table>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Save changes">
        </div>
     </form>
     <?php 
        echo $this->Form->end() 
    ?>
</div>
</div>
</div>
</div>
</div>
