<?php if ($flag == ''): ?>
    <div class="row justify-content-md-center">

        <div class="col-lg-7 boxed">
            <?= $this->Form->create($authorsPublication) ?>
                <legend class="capital-heading"><?= __('Link Editor and Publication') ?></legend>
                <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Editor Name: </td>
                        <td><?php echo $this->Form->control('editor_id', ['label' => '', 'type' => 'text', 'maxLength' => 300, 'list' => 'authorList', 'autocomplete' => 'off']); ?></td>
                    </tr>
                    <tr>
                        <td> Publication ID: </td>
                        <td><?php echo $this->Form->control('publication_id', ['label' => '', 'type' => 'number']); ?></td>
                    </tr>
                    <tr>
                        <td> Sequence: </td>
                        <td><?php $options = [
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                        6 => 6,
                        7 => 7,
                        8 => 8,
                        9 => 9,
                        10 => 10,
                    ];
                    echo $this->Form->control('sequence', ['label' => '', 'type' => 'select', 'options' => $options]); ?></td>
                    </tr>
                    <datalist id="authorList">
                        <?php foreach ($authors_names as $name): ?>    
                            <option value="<?php echo $name ?>" /> 
                        <?php endforeach ; ?>     
                    </datalist>
                </table>

                <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>

        </div>

        <div class="col-lg boxed">
            <div class="capital-heading"><?= __('Related Actions') ?></div>
            <?= $this->Html->link(__('List All Links'), ['action' => 'index'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
            <br/>
            <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
            <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
            <br/>
        </div>

    </div>

<?php elseif ($flag == 'bulk'): ?>

    <?= $this->cell('BulkUpload::confirmation', [
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

    <div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Link'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>

    </div>

    <?= $this->cell('BulkUpload', [
                            'EditorsPublications', 
                            isset($error_list) ? $error_list:null, 
                            isset($header) ? $header:null, 
                            isset($entities) ? $entities:null]); ?>

<?php endif ?>