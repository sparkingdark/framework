<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription[]|\Cake\Collection\CollectionInterface $inscriptions
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Inscription'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="inscriptions index large-9 medium-8 columns content">
    <h3><?= __('Inscriptions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_latest') ?></th>
                <th scope="col"><?= $this->Paginator->sort('atf2conll_diff_resolved') ?></th>
                <th scope="col"><?= $this->Paginator->sort('atf2conll_diff_unresolved') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($inscriptions as $inscription): ?>
            <tr>
                <td><?= $this->Number->format($inscription->id) ?></td>
                <td><?= $inscription->has('artifact') ? $this->Html->link($inscription->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $inscription->artifact->id]) : '' ?></td>
                <td><?= $inscription->has('user') ? $this->Html->link($inscription->user->id, ['controller' => 'Users', 'action' => 'view', $inscription->user->id]) : '' ?></td>
                <td><?= h($inscription->created) ?></td>
                <td><?= $inscription->has('credit') ? $this->Html->link($inscription->credit->id, ['controller' => 'Credits', 'action' => 'view', $inscription->credit->id]) : '' ?></td>
                <td><?= h($inscription->is_latest) ?></td>
                <td><?= h($inscription->atf2conll_diff_resolved) ?></td>
                <td><?= h($inscription->atf2conll_diff_unresolved) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $inscription->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $inscription->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $inscription->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscription->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
</div>
