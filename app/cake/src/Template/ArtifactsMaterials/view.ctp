<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsMaterial $artifactsMaterial
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Material') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsMaterial->has('artifact') ? $this->Html->link($artifactsMaterial->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsMaterial->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Material') ?></th>
                    <td><?= $artifactsMaterial->has('material') ? $this->Html->link($artifactsMaterial->material->id, ['controller' => 'Materials', 'action' => 'view', $artifactsMaterial->material->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Material Color') ?></th>
                    <td><?= $artifactsMaterial->has('material_color') ? $this->Html->link($artifactsMaterial->material_color->id, ['controller' => 'MaterialColors', 'action' => 'view', $artifactsMaterial->material_color->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Material Aspect') ?></th>
                    <td><?= $artifactsMaterial->has('material_aspect') ? $this->Html->link($artifactsMaterial->material_aspect->id, ['controller' => 'MaterialAspects', 'action' => 'view', $artifactsMaterial->material_aspect->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsMaterial->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Material Uncertain') ?></th>
                    <td><?= $artifactsMaterial->is_material_uncertain ? __('Yes') : __('No'); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <!-- <?= $this->Html->link(__('Edit Artifacts Material'), ['action' => 'edit', $artifactsMaterial->id], ['class' => 'btn-action']) ?> -->
        <!-- <?= $this->Form->postLink(__('Delete Artifacts Material'), ['action' => 'delete', $artifactsMaterial->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsMaterial->id), 'class' => 'btn-action']) ?> -->
        <?= $this->Html->link(__('List Artifacts Materials'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifacts Material'), ['action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Materials'), ['controller' => 'Materials', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Material'), ['controller' => 'Materials', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Material Colors'), ['controller' => 'MaterialColors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Material Color'), ['controller' => 'MaterialColors', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Material Aspects'), ['controller' => 'MaterialAspects', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Material Aspect'), ['controller' => 'MaterialAspects', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
    </div>

</div>



